<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Gerashenko
 * Email: v.gershenko1987@gmail.com
 * Date: 02.07.17
 * Time: 16:24
 */
class NameException extends Exception
{
}

abstract class Animal
{
    protected $name;

    public function __construct($name)
    {
        if (!is_string($name)) {
            throw new NameException('"name" must be a string!');
        }

        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}

class Cat extends Animal
{
    public function meow()
    {
        return "Cat {$this->name} is saying meow";
    }
}

try {
    $cat = new Cat('garfield');
} catch (NameException $e) {
    exit($e->getMessage());
}

var_dump($cat->getName() === 'garfield');
var_dump($cat->meow() === 'Cat garfield is saying meow');